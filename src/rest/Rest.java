package rest;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;


public class Rest {

    public static void main(String[] args) {
        Map<Integer, String> database = new HashMap<>();

        String[] requests = {
            "GET /respuestas/hola", // <p>hola mundo</>
            "GET /users",
            "GET /users/1234",
            "GET /users/55556",
            "ABC /users/1234",
            "POST /users username=gbenussi&password=contrasena", // adasdas
            "POST /respuestas/hola body=<p>hola mundo</>",
            "POST /respuestas/hola body=<asdasdasdasdasd",
            "PUT /respuestas/hola title=hola+mundo", // aasdsadasdsa
            "PUT /users/1234 username=giovanni", // aasdsadasdsa
            "DELETE /users/1234",};

        for (int i = 0; i < requests.length; i++) {
            System.out.println("===== ===== ===== ===== =====");

            String request = requests[i];

            String[] tokens = request.split(" ");
            String parametros = tokens[1];

            String http_method = tokens[0];

            String[] tokens_parametros = parametros.split("/");

            String resource = tokens_parametros.length > 1 ? tokens_parametros[1] : "";
            String id = tokens_parametros.length > 2 ? tokens_parametros[2] : "";

            String meta_data = tokens.length > 2 ? tokens[2] : "";

            System.out.println("Consulta: " + request);
            System.out.println("HTTP METHOD: " + http_method);
            System.out.println("Resource: " + resource);
            System.out.println("ID:          " + id);
            System.out.println("META DATA:    " + meta_data);
            switch (http_method) {
                case "GET":
                    if (id == "") {
                        System.out.println("Buscando en la base de datos los ultimos 10 registros de tipo '" + resource + "'");
                        // buscar en el cache
                        // hit o miss
                    } else {
                        System.out.println("Buscando en el cache de '" + resource + "' el registro con id " + id);
                    }
                    break;
                case "POST":
                    System.out.println("Creando un usuario con los siguientes datos: (" + meta_data + ")");
                    for (String params : meta_data.split("&")) {
                        String[] parametros_meta = params.split("=");
                        System.out.println("\t* " + parametros_meta[0] + " -> " + parametros_meta[1]);
                    }
                    break;
                case "PUT":
                    System.out.println("Actualizando el usuario con id " + id + " con los siguientes datos (" + meta_data + ")");
                    for (String params : meta_data.split("&")) {
                        String[] parametros_meta = params.split("=");
                        System.out.println("\t* " + parametros_meta[0] + " -> " + parametros_meta[1]);
                    }
                    break;
                case "DELETE":
                    System.out.println("Borrando el recurso de tipo '" + resource + "' con id " + id);
                    break;
                default:
                    System.out.println("Not a valid HTTP Request");
                    break;
            }
        }
        
        System.out.println("\n\n\nGenerando JSON");
        JSONObject jo = new JSONObject();
        jo.put("firstName", "John");
        jo.put("lastName", "Doe");

        System.out.println(jo.toJSONString());


    }

}
